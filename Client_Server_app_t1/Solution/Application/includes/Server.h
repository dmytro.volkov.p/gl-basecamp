#ifndef SERVER_H_
#define SERVER_H_

#include <WS2tcpip.h>
#include <Windows.h>
#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <mutex>

#pragma comment(lib, "WS2_32.lib")

class Server
{
public:
	Server();
    void SendMessageToClient(int ID, std::vector<std::pair<SOCKET, sockaddr_in>>& _connections);
    void Start();
	~Server();

private:
    SOCKET  Connect = 0;
    std::vector<std::pair<SOCKET, sockaddr_in>> Connections;
    SOCKET  Listen = 0;
    std::string Port = "7770";
    int ClientCount = 0;
    std::string serverCommandsBufer;
    
    void ServerInput();
};


#endif  // SERVER_H_





#ifndef CLIENT_H_
#define CLIENT_H_

#include <WS2tcpip.h>
#include <Windows.h>
#include <iostream>
#include <string>
#include <vector>
#include <thread>  


#pragma comment(lib, "WS2_32.lib")

class Client
{
public:
    Client();
    void Start(const std::string& _addr, std::string _port);
    ~Client();

private:
    SOCKET  Connect = 0;
    std::string command;
    std::string rMessage;
    std::string Addr = "127.0.0.1";
    std::string Port = "7770";
    void receive(SOCKET &_connect, std::string& _rMessage);
};


#endif  // CLIENT_H_

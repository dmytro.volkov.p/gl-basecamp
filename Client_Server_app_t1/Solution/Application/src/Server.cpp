#include "../includes/Server.h"

Server::Server()
{	

}

void Server::SendMessageToClient(int ID, std::vector<std::pair<SOCKET, sockaddr_in>> &_connections)
{

    const unsigned int MAX_BUF_LENGTH = 256;
    char* buffer = new char[MAX_BUF_LENGTH];
    char byeMessage[] = "bye\n" ;

    char host[NI_MAXHOST];		//client's remote name
    char service[NI_MAXHOST];	//service (i.e. port) the client is connect on

    ZeroMemory(host, NI_MAXHOST); 
    ZeroMemory(service, NI_MAXHOST);

    getnameinfo((sockaddr*)&_connections[ID].second,
        sizeof(_connections[ID].second), host, NI_MAXHOST, service, NI_MAXSERV, 0);
    
    while(true) {
        int nsize;
        if (nsize = recv(_connections[ID].first, &buffer[0], MAX_BUF_LENGTH, 0)) {
            std::string rcv_hostName("[" + (std::string)host + "]:");
            std::string rcv;
            for (time_t i = 0; i < nsize; i++) {
                    rcv.push_back(buffer[i]);
            }
            
            if (rcv == "--close")
                break;
            rcv = rcv_hostName + rcv + '\n';
            std::cout << rcv << std::endl;
            
            for (time_t i = 0; i < _connections.size(); i++) {
                send(_connections[i].first, rcv.c_str(), rcv.size(), 0);
            }
        }
    }
    send(_connections[ID].first, byeMessage, strlen(byeMessage), 0);
    Connections.erase(_connections.begin() + ID);
    delete[] buffer;
}

void Server::Start() 
{
    try {
        WSAData data;
        WORD version = MAKEWORD(2, 2);
        int res = WSAStartup(version, &data);
        if (res != 0) return;

        struct addrinfo hints;
        struct addrinfo* result;



        ZeroMemory(&hints, sizeof(hints));

        hints.ai_family = AF_INET;
        hints.ai_flags = AI_PASSIVE;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = IPPROTO_TCP;

        getaddrinfo(NULL, Port.c_str(), &hints, &result);

        Listen = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
        bind(Listen, result->ai_addr, result->ai_addrlen);
        listen(Listen, SOMAXCONN);

        freeaddrinfo(result);
        std::cout << "Start server" << std::endl;
        char m_connect[] = "Connect";

        sockaddr_in client;
        int clientsize = sizeof(client);
        std::thread t1(&Server::ServerInput, *this);
        std::vector<std::thread> listOfThreads;
        while (serverCommandsBufer != "--stop")
        {
           
            if (Connect = accept(Listen, (sockaddr*)&client, &clientsize)) {
                bool hasCurrentClient = false;
                for (time_t i = 0; i < Connections.size(); i++)
                    if (Connections[i].first == Connect) 
                        hasCurrentClient = true;
                if (hasCurrentClient)
                    break;
                std::cout << "Client connect" << std::endl;
                Connections.push_back(std::make_pair(Connect, client));

                send(Connections.back().first, m_connect, strlen(m_connect), 0);
                
                listOfThreads.emplace_back(std::thread(&Server::SendMessageToClient, *this, Connections.size() - 1,std::ref(Connections)));

                
            }

        }
        if (t1.joinable())
            t1.join();
        for (time_t i = 0; i < listOfThreads.size(); i++) {
            if (listOfThreads[i].joinable())
                listOfThreads[i].join();
        }
    }
    catch (std::exception ex)
    {
        std::cout << ex.what() << std::endl;
    }
}



Server::~Server()
{
}

void Server::ServerInput()
{
    while (serverCommandsBufer != "--stop")
    {
        std::cin >> serverCommandsBufer;
    }
}

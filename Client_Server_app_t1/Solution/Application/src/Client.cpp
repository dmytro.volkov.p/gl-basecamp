#include "..\includes\Client.h"

Client::Client()
{
    
}


void Client::Start(const std::string& _addr, std::string _port)
{
    std::thread t1(&Client::receive, *this, std::ref(Connect) ,std::ref(rMessage));
    const unsigned int MAX_BUF_LENGTH = 256;
    WSADATA wsaData;
    struct addrinfo* result = NULL,
        * ptr = NULL,
        hints;
    int iResult;
    int recvbuflen = MAX_BUF_LENGTH;

    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {

        printf("WSAStartup failed with error: %d\n", iResult);
        t1.join();
        return;
        
    }

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
    iResult = getaddrinfo(_addr.c_str(), _port.c_str(), &hints, &result);
    if (iResult != 0) {
        printf("getaddrinfo failed with error: %d\n", iResult);
        t1.join();
        WSACleanup();
        return;
    }

    // Attempt to connect to an address until one succeeds
    for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

        // Create a SOCKET for connecting to server
        Connect = socket(ptr->ai_family, ptr->ai_socktype,
            ptr->ai_protocol);
        if (Connect == INVALID_SOCKET) {
            printf("socket failed with error: %ld\n", WSAGetLastError());
            t1.join();
            WSACleanup();
            return;
        }

        // Connect to server.
        iResult = connect(Connect, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(Connect);
            Connect = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (Connect == INVALID_SOCKET) {
        printf("Unable to connect to server!\n");
        t1.join();
        WSACleanup();
        return;
    }

    
    while (command != "--stop")
    {
        std::cin >> command;
        iResult = send(Connect, command.c_str(), (int)strlen(command.c_str()), 0);
        if (iResult == SOCKET_ERROR) {
            printf("send failed with error: %d\n", WSAGetLastError());

        }

    }
    


    // cleanup
    t1.join();
    closesocket(Connect);
    WSACleanup();
}

Client::~Client()
{
}

void Client::receive(SOCKET &_connect, std::string& _rMessage)
{
    const unsigned int MAX_BUF_LENGTH = 256;
    char* buffer = new char[MAX_BUF_LENGTH];
    while (command != "--stop")
    {
        _rMessage.clear();
        int nsize;
        if ( nsize = recv(_connect, &buffer[0], MAX_BUF_LENGTH, 0))
        {
            for (time_t i = 0; i < nsize; i++) {
                _rMessage.push_back(buffer[i]);
            }
            std::cout << _rMessage << std::endl;
          
        }
    }
}

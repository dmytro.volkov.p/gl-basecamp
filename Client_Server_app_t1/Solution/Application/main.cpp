#include "includes/Server.h"
#include "includes/Client.h"
#include <thread>


int main()
{
	std::string mode;
	std::cin >> mode;
	if (mode == "server") {
		Server tcpServer;
		tcpServer.Start();
	}
	if (mode == "client")
	{
		std::string addr;
		std::string port;
		std::cin >> addr >> port;
		Client tcpClient;
		tcpClient.Start(addr,port);
	}
	return 0;
}
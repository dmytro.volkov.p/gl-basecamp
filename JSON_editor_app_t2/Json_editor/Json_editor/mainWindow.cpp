#include <Windows.h>
#include "Json/Json.h"

static Json JsonEdit;

#include "mainWindow.h"

using namespace Jsoneditor;

[System::STAThread] int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	Application::Run(gcnew mainWindow);
	return 0;
}

std::string ReplaceAll(std::string str, const std::string& from, const std::string& to) {
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); 
	}
	return str;
}

void MarshalString(System::String^ s, std::string& os) {
	using namespace Runtime::InteropServices;
	const char* chars =
		(const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
	os = chars;
	Marshal::FreeHGlobal(IntPtr((void*)chars));
}

void mainWindow::updateTextBox() {
	std::string bufStr = JsonEdit.gerRaw().c_str();
	bufStr = ReplaceAll(bufStr, "\n", "\r\n");
	String^ clistr_text = gcnew String(bufStr.c_str());
	std::string str_lines = "Lines: ";
	str_lines += std::to_string(JsonEdit.getCountOfLines());
	String^ clistr_lines = gcnew String(str_lines.c_str());
	richTextBox1->Text = clistr_text;
	tsslLines->Text = clistr_lines;
	tsslSymbols->Text = "Symbols: " + richTextBox1->Text->Length.ToString();
	delete clistr_text;
	delete clistr_lines;
	AddLineNumbers();
}

void mainWindow::updateTreeView() {
	treeView1->Nodes->Clear();
	for (auto& _map : JsonEdit.getVector()) {
		TreeNode^ node = gcnew TreeNode(gcnew String(_map.first.c_str()));
		treeView1->Nodes->Add(node);
		for (auto& field : _map.second) {
			TreeNode^ subnode = gcnew TreeNode(gcnew String(field.first.c_str()));
			node->Nodes->Add(subnode);
		}
	}
	AddLineNumbers();
}
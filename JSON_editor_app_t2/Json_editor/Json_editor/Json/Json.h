#ifndef JSON_H
#define JSON_H

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>

using map = std::map<std::string, std::string>;

class Json
{
public:
	Json();
	//Ctor.  _fileName - path to file that will be used
	Json(const std::string& _fileName);
	//add an object to container
	void addObject(const std::string& _ObjName);
	//remove an object from container (First finded object with name _ObjNamme) 
	void rmObject(const std::string& _ObjName);
	//add field with key _first and value _second to every objects
	void addField(const std::string& _first, const std::string& _second);
	//remove field with name _Field from every objects
	void rmField(const std::string& _Field);
	//add key _first with data _second to _Object
	void addField(const std::string& _Object,const std::string& _first, const std::string& _second);
	//save data from file to Json class. _fileName - path to file that used
	void readFile(const std::string& _fileName);
	//encode data from vector<string,map> to text;
	void encodeJson();
	//decode data from raw text to vector<string,map>
	void decodeJson();
	//getting status from error buffer
	const std::string getStatus() const;
	//getting string of data
	const std::string gerRaw() const;
	//save json formated data to file. _fileName - path to file that used or new file path
	void writeFile(const std::string& _fileName);
	//empty status of Json container
	bool isEmpty() const;
	//getting fields count of first object in container
	int getFieldsCount() const;
	//getiing count of "\n" from raw text 
	int getCountOfLines() const;
	//getting reference to container (protected from changes)
	const std::vector<std::pair<std::string, map>>& getVector() const;
	//Dtor
	~Json();
private:
	//Json text of container 
	std::string m_rawText;
	//container for Json data
	std::vector<std::pair<std::string,map>> m_conteiner;
	//error buffer
	std::string m_errorBuf;
};
#endif // JSON_H

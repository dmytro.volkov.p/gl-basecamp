#include "Json.h"

Json::Json()
{
}

Json::Json(const std::string& _fileName)
{
	readFile(_fileName);
}

void Json::addObject(const std::string& _ObjName)
{
	m_conteiner.push_back(std::make_pair(_ObjName, map()));	
}

void Json::rmObject(const std::string& _ObjName)
{
	if (!m_conteiner.empty()) {
		auto it = m_conteiner.begin();
		while (it != m_conteiner.end())
		{
			if (it->first == _ObjName) {
				m_conteiner.erase(it);
				return;
			}
			it++;
		}
	}
}

void Json::addField(const std::string& _first, const std::string& _second)
{
	if (!m_conteiner.empty()) {
		for(size_t i = 0; i < m_conteiner.size(); ++i)
			m_conteiner[i].second.emplace( _first, _second );
	}
}

void Json::rmField(const std::string& _Field)
{
	if (!m_conteiner.empty()) {
		for (size_t i = 0; i < m_conteiner.size(); ++i)
			m_conteiner[i].second.erase(_Field);
	}
}

void Json::addField(const std::string& _Object, const std::string& _first, const std::string& _second)
{
	if (!m_conteiner.empty()) {
		for (size_t i = 0; i < m_conteiner.size(); ++i)
			if (m_conteiner[i].first == _Object) {
				m_conteiner[i].second.emplace(_first, _second);
				return;
			}
	}
}

void Json::readFile(const std::string& _fileName)
{
	m_rawText.erase();
	std::ifstream File(_fileName, std::ios::in);
	std::string line;
	if (!File.is_open())
	{
		m_errorBuf = "File opening error";
		return;
	}
	while (std::getline(File, line))
	{
		m_rawText += line;
		m_rawText += '\n';
	}
	File.close();
}

void Json::encodeJson()
{
	if (!m_conteiner.empty())
	{
		m_rawText.clear();
		m_rawText += "{\n";
		for (int i = 0; i < m_conteiner.size(); ++i) {

			std::map<std::string, std::string>::iterator it = m_conteiner[i].second.begin();
			m_rawText += "\t" + m_conteiner[i].first + " : {\n";
			while (it != m_conteiner[i].second.end())
			{
				m_rawText += "\t\t\"" + it->first + "\" : \"" + it->second + "\" ,\n";
				it++;
			}
			if(!m_conteiner[i].second.empty())m_rawText.pop_back();
			m_rawText.pop_back();
			m_rawText.push_back('\n');
			m_rawText += "\t}\n";
		}
		m_rawText += "}\n";
	}
}

void Json::decodeJson()
{
	// ...
}

const std::string Json::getStatus() const
{
	return m_errorBuf;
}

const std::string Json::gerRaw() const
{
	return m_rawText;
}

void Json::writeFile(const std::string& _fileName)
{
	std::ofstream outFile(_fileName, std::ios::out);
	if (!outFile.is_open())
	{
		m_errorBuf = "File opening error";
		return;
	}
	else
	{
		if (!m_conteiner.empty()) 
		{
			encodeJson();
			outFile << m_rawText;
		}
	}
	outFile.close();
}

bool Json::isEmpty() const
{
	return m_conteiner.empty();
}

int Json::getFieldsCount() const
{
	if(!m_conteiner.empty())
		return m_conteiner[0].second.size();
	return -1;
}

int Json::getCountOfLines() const
{
	int lines = 0;
	for (size_t i = 0; i < m_rawText.size(); ++i)
		if (m_rawText[i] == '\n')
			lines++;
	return lines;
}

const std::vector<std::pair<std::string, map>>& Json::getVector() const
{
	return m_conteiner;
}

Json::~Json()
{
}

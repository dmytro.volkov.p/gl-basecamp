#pragma once
#include <string>
#include <iostream>
#include <algorithm>

std::string ReplaceAll(std::string str, const std::string& from, const std::string& to);
void MarshalString(System::String^ s, std::string& os);


namespace Jsoneditor {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	/// <summary>
	/// Summary for mainWindow
	/// </summary>
	public ref class mainWindow : public System::Windows::Forms::Form
	{
	public:
		mainWindow(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~mainWindow()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^ menuStrip1;
	protected:
	private: System::Windows::Forms::ToolStripMenuItem^ fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^ openToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^ saveToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^ exitToolStripMenuItem;
	private: System::Windows::Forms::StatusStrip^ statusStrip1;
	private: System::Windows::Forms::ToolStripStatusLabel^ toolStripStatusLabel1;
	private: System::Windows::Forms::SplitContainer^ splitContainer1;

	private: System::Windows::Forms::ToolStripStatusLabel^ tsslLines;

	private: System::Windows::Forms::Button^ addField;
	private: System::Windows::Forms::Button^ addObject;

	private: System::Windows::Forms::TreeView^ treeView1;
	private: System::Windows::Forms::RichTextBox^ richTextBox1;
	private: System::Windows::Forms::RichTextBox^ LineNumberTextBox;
	private: System::Windows::Forms::ToolStripStatusLabel^ tsslSymbols;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::TextBox^ tbObjectName;
	private: System::Windows::Forms::TextBox^ tbFieldName;
	private: System::Windows::Forms::TextBox^ tbData;
	private: System::Windows::Forms::Button^ btDelField;
	private: System::Windows::Forms::Button^ btDelObject;




	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->openToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
			this->toolStripStatusLabel1 = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->tsslLines = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->tsslSymbols = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->splitContainer1 = (gcnew System::Windows::Forms::SplitContainer());
			this->btDelField = (gcnew System::Windows::Forms::Button());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->tbObjectName = (gcnew System::Windows::Forms::TextBox());
			this->tbFieldName = (gcnew System::Windows::Forms::TextBox());
			this->tbData = (gcnew System::Windows::Forms::TextBox());
			this->addField = (gcnew System::Windows::Forms::Button());
			this->addObject = (gcnew System::Windows::Forms::Button());
			this->treeView1 = (gcnew System::Windows::Forms::TreeView());
			this->LineNumberTextBox = (gcnew System::Windows::Forms::RichTextBox());
			this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
			this->btDelObject = (gcnew System::Windows::Forms::Button());
			this->menuStrip1->SuspendLayout();
			this->statusStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer1))->BeginInit();
			this->splitContainer1->Panel1->SuspendLayout();
			this->splitContainer1->Panel2->SuspendLayout();
			this->splitContainer1->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->fileToolStripMenuItem });
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(819, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->openToolStripMenuItem,
					this->saveToolStripMenuItem, this->exitToolStripMenuItem
			});
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(37, 20);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// openToolStripMenuItem
			// 
			this->openToolStripMenuItem->Name = L"openToolStripMenuItem";
			this->openToolStripMenuItem->Size = System::Drawing::Size(103, 22);
			this->openToolStripMenuItem->Text = L"Open";
			this->openToolStripMenuItem->Click += gcnew System::EventHandler(this, &mainWindow::openToolStripMenuItem_Click);
			// 
			// saveToolStripMenuItem
			// 
			this->saveToolStripMenuItem->Name = L"saveToolStripMenuItem";
			this->saveToolStripMenuItem->Size = System::Drawing::Size(103, 22);
			this->saveToolStripMenuItem->Text = L"Save";
			this->saveToolStripMenuItem->Click += gcnew System::EventHandler(this, &mainWindow::saveToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->Size = System::Drawing::Size(103, 22);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &mainWindow::exitToolStripMenuItem_Click);
			// 
			// statusStrip1
			// 
			this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->toolStripStatusLabel1,
					this->tsslLines, this->tsslSymbols
			});
			this->statusStrip1->Location = System::Drawing::Point(0, 436);
			this->statusStrip1->Name = L"statusStrip1";
			this->statusStrip1->Size = System::Drawing::Size(819, 22);
			this->statusStrip1->TabIndex = 1;
			this->statusStrip1->Text = L"statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this->toolStripStatusLabel1->Name = L"toolStripStatusLabel1";
			this->toolStripStatusLabel1->Size = System::Drawing::Size(43, 17);
			this->toolStripStatusLabel1->Text = L"Errors: ";
			// 
			// tsslLines
			// 
			this->tsslLines->Name = L"tsslLines";
			this->tsslLines->Size = System::Drawing::Size(40, 17);
			this->tsslLines->Text = L"Lines: ";
			// 
			// tsslSymbols
			// 
			this->tsslSymbols->Name = L"tsslSymbols";
			this->tsslSymbols->Size = System::Drawing::Size(55, 17);
			this->tsslSymbols->Text = L"Symbols:";
			// 
			// splitContainer1
			// 
			this->splitContainer1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->splitContainer1->Location = System::Drawing::Point(0, 24);
			this->splitContainer1->Name = L"splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this->splitContainer1->Panel1->Controls->Add(this->btDelObject);
			this->splitContainer1->Panel1->Controls->Add(this->btDelField);
			this->splitContainer1->Panel1->Controls->Add(this->label3);
			this->splitContainer1->Panel1->Controls->Add(this->label2);
			this->splitContainer1->Panel1->Controls->Add(this->label1);
			this->splitContainer1->Panel1->Controls->Add(this->tbObjectName);
			this->splitContainer1->Panel1->Controls->Add(this->tbFieldName);
			this->splitContainer1->Panel1->Controls->Add(this->tbData);
			this->splitContainer1->Panel1->Controls->Add(this->addField);
			this->splitContainer1->Panel1->Controls->Add(this->addObject);
			this->splitContainer1->Panel1->Controls->Add(this->treeView1);
			// 
			// splitContainer1.Panel2
			// 
			this->splitContainer1->Panel2->Controls->Add(this->LineNumberTextBox);
			this->splitContainer1->Panel2->Controls->Add(this->richTextBox1);
			this->splitContainer1->Size = System::Drawing::Size(819, 412);
			this->splitContainer1->SplitterDistance = 299;
			this->splitContainer1->TabIndex = 2;
			// 
			// btDelField
			// 
			this->btDelField->Location = System::Drawing::Point(221, 389);
			this->btDelField->Name = L"btDelField";
			this->btDelField->Size = System::Drawing::Size(75, 20);
			this->btDelField->TabIndex = 9;
			this->btDelField->Text = L"Del Field";
			this->btDelField->UseVisualStyleBackColor = true;
			this->btDelField->Click += gcnew System::EventHandler(this, &mainWindow::btDelField_Click);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(12, 392);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(33, 13);
			this->label3->TabIndex = 8;
			this->label3->Text = L"Data:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 366);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(61, 13);
			this->label2->TabIndex = 7;
			this->label2->Text = L"Field name:";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 339);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(70, 13);
			this->label1->TabIndex = 6;
			this->label1->Text = L"Oblect name:";
			// 
			// tbObjectName
			// 
			this->tbObjectName->Location = System::Drawing::Point(88, 336);
			this->tbObjectName->Name = L"tbObjectName";
			this->tbObjectName->Size = System::Drawing::Size(127, 20);
			this->tbObjectName->TabIndex = 5;
			// 
			// tbFieldName
			// 
			this->tbFieldName->Location = System::Drawing::Point(88, 363);
			this->tbFieldName->Name = L"tbFieldName";
			this->tbFieldName->Size = System::Drawing::Size(127, 20);
			this->tbFieldName->TabIndex = 4;
			// 
			// tbData
			// 
			this->tbData->Location = System::Drawing::Point(88, 389);
			this->tbData->Name = L"tbData";
			this->tbData->Size = System::Drawing::Size(127, 20);
			this->tbData->TabIndex = 3;
			// 
			// addField
			// 
			this->addField->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->addField->Location = System::Drawing::Point(221, 363);
			this->addField->Name = L"addField";
			this->addField->Size = System::Drawing::Size(75, 20);
			this->addField->TabIndex = 2;
			this->addField->Text = L"Add Field";
			this->addField->UseVisualStyleBackColor = true;
			this->addField->Click += gcnew System::EventHandler(this, &mainWindow::addField_Click);
			// 
			// addObject
			// 
			this->addObject->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->addObject->Location = System::Drawing::Point(222, 334);
			this->addObject->Name = L"addObject";
			this->addObject->Size = System::Drawing::Size(38, 23);
			this->addObject->TabIndex = 1;
			this->addObject->Text = L"Add";
			this->addObject->UseVisualStyleBackColor = true;
			this->addObject->Click += gcnew System::EventHandler(this, &mainWindow::addObject_Click);
			// 
			// treeView1
			// 
			this->treeView1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->treeView1->Location = System::Drawing::Point(3, 3);
			this->treeView1->Name = L"treeView1";
			this->treeView1->Size = System::Drawing::Size(293, 324);
			this->treeView1->TabIndex = 0;
			// 
			// LineNumberTextBox
			// 
			this->LineNumberTextBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left));
			this->LineNumberTextBox->BackColor = System::Drawing::Color::Silver;
			this->LineNumberTextBox->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->LineNumberTextBox->Cursor = System::Windows::Forms::Cursors::PanNE;
			this->LineNumberTextBox->ForeColor = System::Drawing::Color::Black;
			this->LineNumberTextBox->Location = System::Drawing::Point(0, 3);
			this->LineNumberTextBox->Name = L"LineNumberTextBox";
			this->LineNumberTextBox->ReadOnly = true;
			this->LineNumberTextBox->ScrollBars = System::Windows::Forms::RichTextBoxScrollBars::None;
			this->LineNumberTextBox->Size = System::Drawing::Size(33, 406);
			this->LineNumberTextBox->TabIndex = 2;
			this->LineNumberTextBox->Text = L"";
			// 
			// richTextBox1
			// 
			this->richTextBox1->AcceptsTab = true;
			this->richTextBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->richTextBox1->EnableAutoDragDrop = true;
			this->richTextBox1->Location = System::Drawing::Point(39, 3);
			this->richTextBox1->Name = L"richTextBox1";
			this->richTextBox1->Size = System::Drawing::Size(474, 406);
			this->richTextBox1->TabIndex = 1;
			this->richTextBox1->Text = L"";
			this->richTextBox1->SelectionChanged += gcnew System::EventHandler(this, &mainWindow::richTextBox1_SelectionChanged);
			this->richTextBox1->VScroll += gcnew System::EventHandler(this, &mainWindow::richTextBox1_VScroll);
			this->richTextBox1->FontChanged += gcnew System::EventHandler(this, &mainWindow::richTextBox1_FontChanged);
			this->richTextBox1->TextChanged += gcnew System::EventHandler(this, &mainWindow::richTextBox1_TextChanged);
			// 
			// btDelObject
			// 
			this->btDelObject->Location = System::Drawing::Point(261, 334);
			this->btDelObject->Name = L"btDelObject";
			this->btDelObject->Size = System::Drawing::Size(35, 23);
			this->btDelObject->TabIndex = 10;
			this->btDelObject->Text = L"Del";
			this->btDelObject->UseVisualStyleBackColor = true;
			this->btDelObject->Click += gcnew System::EventHandler(this, &mainWindow::btDelObject_Click);
			// 
			// mainWindow
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(819, 458);
			this->Controls->Add(this->splitContainer1);
			this->Controls->Add(this->statusStrip1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"mainWindow";
			this->Text = L"Json editor";
			this->Load += gcnew System::EventHandler(this, &mainWindow::mainWindow_Load);
			this->Resize += gcnew System::EventHandler(this, &mainWindow::mainWindow_Resize);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->statusStrip1->ResumeLayout(false);
			this->statusStrip1->PerformLayout();
			this->splitContainer1->Panel1->ResumeLayout(false);
			this->splitContainer1->Panel1->PerformLayout();
			this->splitContainer1->Panel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer1))->EndInit();
			this->splitContainer1->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		public: int getWidth()
		{
			int w = 25;
			// get total lines of richTextBox1    
			int line = richTextBox1->Lines->Length;

			if (line <= 99)
			{
				w = 20 + (int)richTextBox1->Font->Size;
			}
			else if (line <= 999)
			{
				w = 30 + (int)richTextBox1->Font->Size;
			}
			else
			{
				w = 50 + (int)richTextBox1->Font->Size;
			}

			return w;
		}
		public: void AddLineNumbers()
		{
			// create & set Point pt to (0,0)    
			Point pt(0, 0);
			// get First Index & First Line from richTextBox1    
			int First_Index = richTextBox1->GetCharIndexFromPosition(pt);
			int First_Line = richTextBox1->GetLineFromCharIndex(First_Index);
			// set X & Y coordinates of Point pt to ClientRectangle Width & Height respectively    
			pt.X = ClientRectangle.Width;
			pt.Y = ClientRectangle.Height;
			// get Last Index & Last Line from richTextBox1    
			int Last_Index = richTextBox1->GetCharIndexFromPosition(pt);
			int Last_Line = richTextBox1->GetLineFromCharIndex(Last_Index);
			// set Center alignment to LineNumberTextBox    
			LineNumberTextBox->SelectionAlignment = HorizontalAlignment::Center;
			// set LineNumberTextBox text to null & width to getWidth() function value    
			LineNumberTextBox->Text = "";
			LineNumberTextBox->Width = getWidth();
			// now add each line number to LineNumberTextBox upto last line    
			for (int i = First_Line; i <= Last_Line + 2; i++)
			{
				LineNumberTextBox->Text += i + 1 + "\n";
			}
		}

		private: System::Void mainWindow_Load(System::Object^ sender, System::EventArgs^ e) {
			LineNumberTextBox->Font = richTextBox1->Font;
			richTextBox1->Select();
			AddLineNumbers();
		}
		private: System::Void mainWindow_Resize(System::Object^ sender, System::EventArgs^ e) {
			AddLineNumbers();
		}

		void updateTextBox();

		void updateTreeView();

		private: System::Void exitToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e) {
			this->Close();
		}

		private: System::Void openToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e) {
			std::string path;
			OpenFileDialog^ openDlg = gcnew OpenFileDialog();
			openDlg->Filter = "json files (*.json)|*.json|txt files (*.txt)|*.txt|All files (*.*)|*.*";
			
			if (System::Windows::Forms::DialogResult::OK == openDlg->ShowDialog())
			{
				String^ fileName = openDlg->FileName;
				MarshalString(fileName, path);
			}
			JsonEdit.readFile(path);
			if (JsonEdit.getStatus().empty()) {
				std::string bufStr = JsonEdit.gerRaw().c_str();
				bufStr = ReplaceAll(bufStr,"\n","\r\n");
				String^ clistr = gcnew String(bufStr.c_str());
				richTextBox1->Text = clistr;
				delete clistr;
			}
			else {
				String^ clistr = gcnew String(JsonEdit.getStatus().c_str());
				statusStrip1->Text = clistr;
				delete clistr;
			}
			delete openDlg;
		}

		private: System::Void saveToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e){
			std::string path;
			SaveFileDialog^ saveDlg = gcnew SaveFileDialog();
			saveDlg->Filter = "json files (*.json)|*.json|txt files (*.txt)|*.txt";
			if (System::Windows::Forms::DialogResult::OK == saveDlg->ShowDialog())
			{
				String^ fileName = saveDlg->FileName;
				MarshalString(fileName, path);
				delete fileName;
			}
			JsonEdit.writeFile(path);
			if (!JsonEdit.getStatus().empty()) 
			{
				String^ clistr = gcnew String(JsonEdit.getStatus().c_str());
				statusStrip1->Text = clistr;
				delete clistr;
			}
			delete saveDlg;
		}
		private: System::Void addObject_Click(System::Object^ sender, System::EventArgs^ e) {
			if (tbObjectName->Text != "")
			{
				std::string ObjName;
				MarshalString(tbObjectName->Text, ObjName);
				JsonEdit.addObject(ObjName);
			}
			else {
				JsonEdit.addObject("New Object");
			}
			JsonEdit.encodeJson();
			updateTextBox();
			updateTreeView();
		}
		private: System::Void addField_Click(System::Object^ sender, System::EventArgs^ e) {
			if (!JsonEdit.isEmpty()) {
				std::string ObjName;
				std::string FieldName;
				std::string Data;
				MarshalString(tbObjectName->Text, ObjName);
				MarshalString(tbFieldName->Text, FieldName);
				MarshalString(tbData->Text, Data);
				if (tbObjectName->Text != "") {
					if (tbFieldName->Text != "" && tbData->Text != "") {
						JsonEdit.addField(ObjName,FieldName, Data);
					}
				}
				else if (tbFieldName->Text != "")
					JsonEdit.addField(FieldName, "Data");
				else
					JsonEdit.addField("Field" + std::to_string(JsonEdit.getFieldsCount() + 1), "Data");
			}
			JsonEdit.encodeJson();
			updateTextBox();
			updateTreeView();
		}
	private: System::Void richTextBox1_SelectionChanged(System::Object^ sender, System::EventArgs^ e) {
		Point pt = richTextBox1->GetPositionFromCharIndex(richTextBox1->SelectionStart);
		if (pt.X == 1)
		{
			AddLineNumbers();
		}
	}

	private: System::Void richTextBox1_VScroll(System::Object^ sender, System::EventArgs^ e) {
		LineNumberTextBox->Text = "";
		AddLineNumbers();
		LineNumberTextBox->Invalidate();
	}

	private: System::Void richTextBox1_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		if (richTextBox1->Text == "")
		{
			AddLineNumbers();
		}
	}
	private: System::Void richTextBox1_FontChanged(System::Object^ sender, System::EventArgs^ e) {
		LineNumberTextBox->Font = richTextBox1->Font;
		richTextBox1->Select();
		AddLineNumbers();
	}
	private: System::Void btDelField_Click(System::Object^ sender, System::EventArgs^ e) {
		if (tbFieldName->Text != "") {
			std::string FieldName;
			MarshalString(tbFieldName->Text, FieldName);
			JsonEdit.rmField(FieldName);
		}
		JsonEdit.encodeJson();
		updateTextBox();
		updateTreeView();
	}
	private: System::Void btDelObject_Click(System::Object^ sender, System::EventArgs^ e) {
		if (tbObjectName->Text != "") {
			std::string ObjectName;
			MarshalString(tbObjectName->Text, ObjectName);
			JsonEdit.rmObject(ObjectName);
		}
		JsonEdit.encodeJson();
		updateTextBox();
		updateTreeView();
	}
};
}


